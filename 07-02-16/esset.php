<?php
$var ='';


// this will evaluate to true so the text will be printed. if (isset($ver) ) {
echo "thi ver is set so i will print.";



// in the next examples we'll use var_dump to output
// the return value of isset () .
 $a = "test";
 $b = "another test";
 
 ver_dump(isset($a));   // TRUE
 ver_dump(isset($a, $b));  //TRUE 
 
 unset ($a);
 
 var_dump(isset($a));    //FALSE
 ver_dump(isset($a, $b));  // FALSE
 
 
 $foo = NULL;
 ver_dump(isset($foo));  // FALSE
 
 ?>